/****************************************************************************
 * genradio/gr/gr_main.c
 *
 *   Copyright (C) 2017 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet <sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <nuttx/wireless/generic/genradio.h>
#include "genradio/genradio.h"

static const char *g_modnames[] = 
{
  "fsk",   /* RADIO_MOD_2FSK  Frequency shift keying */
  "gfsk",  /* RADIO_MOD_2GFSK Gaussian Frequency shift keying */
  "4fsk",  /* RADIO_MOD_4FSK  Frequency shift keying, 4 tones */
  "4gfsk", /* RADIO_MOD_4GFSK Gaussian Frequency shift keying, 4 tones */
  "cw",    /* RADIO_MOD_CW    Continuous wave modulation (for testing) */
  "ask",   /* RADIO_MOD_ASK   Amplitude shift keying */
  "ook",   /* RADIO_MOD_OOK   On-Off keying */
  "msk",   /* RADIO_MOD_MSK   Minimum Shift Keying */
  "gmsk",  /* RADIO_MOD_GMSK  Gaussian Minimum Shift Keying */
  "bpsk",  /* RADIO_MOD_BPSK  Binary Phase Shift Keying */
  "qpsk",  /* RADIO_MOD_QPSK  Quadrature Phase Shift Keying */
  "lora",  /* RADIO_MOD_LORA  Proprietary chirped LoRa modulation */
};

/****************************************************************************
 * Name: gr_status
 *
 * Description:
 *   Display the generic radio device status
 *
 ****************************************************************************/

static uint8_t sw[GENRADIO_SYNCWORD_MAXBYTES]; //room for 8*8 = 64 sync word bits

static int gr_status(char *devname, int fd)
{
  int ret;

  uint32_t bfreq=0;
  uint32_t chspc=0;
  int32_t  chan=0;
  uint32_t mod=0;
  uint32_t mod_index=0;
  uint32_t mod_dev=0;
  uint32_t rate=0;
  uint32_t bw=0;
  uint32_t big;
  uint32_t pl;
  uint32_t swl;
  int32_t  txp;
  int i;
  uint64_t freq;

  ret = genradio_getbasefreq(fd, &bfreq);
  if(ret == 0)
    {
      ret = genradio_getchanspacing(fd, &chspc);
    }
  if(ret == 0)
    {
      ret = genradio_getchannel(fd, &chan);
    }
  if(ret == 0)
    {
      ret = genradio_getmodulation(fd, &mod);
    }

  if(ret ==0)
    {
      if(mod < RADIO_MOD_FSK_LAST)
        {
          ret = genradio_getdeviation(fd, &mod_dev);
        }
      else if(mod==RADIO_MOD_ASK)
        {
          ret = genradio_getmodulationindex(fd, &mod_index);
        }
    }

  if(ret == 0)
    {
      ret = genradio_getdatarate(fd, &rate);
    }

  if(ret == 0)
    {
      ret = genradio_getrxbandwidth(fd, &bw);
    }

  if(ret == 0)
    {
      ret = genradio_gettxpower(fd, &txp);
    }

  if(ret == 0)
    {
      ret = genradio_getpreamblelen(fd, &pl);
    }

  if(ret == 0)
    {
      swl = sizeof(sw) << 3;
      ret = genradio_getsyncword(fd, sw, &swl);
    }

  if(ret != 0)
    {
      return 1; /* something failed */
    }

  printf("Interface status for %s\n", devname);

  freq = (uint64_t)bfreq * 1000LLU;
  freq += (uint64_t)chan * (uint64_t)chspc;

  printf("Base Frequency :");
  if(bfreq>999999)
    {
      big    = bfreq/1000000;
      bfreq -= big * 1000000;
      printf("%d ", big);
    }
  big    = bfreq/1000;
  bfreq -= big * 1000;
  printf("%03d %03d Hz\n", big, bfreq);

  big    = chspc/1000;
  chspc -= (big*1000);
  printf("Channel spacing: %lu.%03lu Hz\n", big, chspc);

  printf("Channel        : %lu (%llu.%03llu Hz)\n", chan, freq/1000, freq%1000);

  if(mod < RADIO_MOD_COUNT)
    {
      printf("Modulation    : %s\n", g_modnames[mod]);
    }
  else
    {
      printf("Modulation    : Unknown (%d)\n",mod);
    }
  if(mod==RADIO_MOD_ASK)
    {
      printf("Mod index     : %lu ppm\n",mod_index);
    }
  else if(mod<RADIO_MOD_FSK_LAST)
    {
      big      = mod_dev/1000;
      mod_dev -= (big*1000);
      printf("Deviation     : %lu.%03lu Hz\n", big, mod_dev);
    }

  big   = rate/1000;
  rate -= (big*1000);
  printf("Data rate     : %lu.%03lu bps\n", big, rate);

  big = bw/1000;
  bw -= (big*1000);
  printf("RX bandwidth  : %lu.%03lu Hz\n", big, bw);

  big  = txp/100;
  txp -= (big*100);
  printf("TX power      : %ld.%02lu dBm\n", big, txp);
  
  printf("Preamble len: %lu bits, sync word: %d bits [", pl, swl);
  for(i = 0; i < BYTESFORBITS(swl); i++)
    {
      printf("%02X", sw[i]);
    }
  printf("]\n");
  return 0;
}

/****************************************************************************
 * Name: gr_freq
 *
 * Description:
 *   Define the base tx/rx frequency of the generic radio device.
 *
 ****************************************************************************/

static void gr_freq(int fd, char *arg)
{
  uint32_t freq;
  int err;
  char *endptr;

  err = errno;
  set_errno(0);
  
  freq = strtoul(arg, &endptr, 10);

  if(errno != 0)
    {
      fprintf(stderr, "invalid number\n");
      return;
    }

  set_errno(err);

  if(*endptr && *endptr=='k')
    {
      freq *= 1000;
    }

  if(*endptr && *endptr=='M')
    {
      freq *= 1000000;
    }

  genradio_setbasefreq(fd, freq); /* Hz */
}

/****************************************************************************
 * Name: gr_chspc
 *
 * Description:
 *   Define the base tx/rx frequency of the generic radio device.
 *
 ****************************************************************************/

static void gr_chspc(int fd, char *arg)
{
  uint32_t freq;
  int err;
  char *endptr;

  err = errno;
  set_errno(0);
  
  freq = strtoul(arg, &endptr, 10);

  if(errno != 0)
    {
      fprintf(stderr, "invalid number\n");
      return;
    }

  set_errno(err);

  if(*endptr && *endptr=='k')
    {
      freq *= 1000;
    }

  if(freq>4000000)
    {
      fprintf(stderr, "error: max value 4 MHz\n");
      return;
    }

  genradio_setchanspacing(fd, freq*1000); /* mHz */
}

/****************************************************************************
 * Name: gr_chan
 *
 * Description:
 *   Define the radio channel
 *
 ****************************************************************************/
static void gr_chan(int fd, char *arg)
{
  int32_t chan;
  int err;
  char *endptr;

  err = errno;
  set_errno(0);
  
  chan = strtol(arg, &endptr, 10);

  if(errno != 0)
    {
      fprintf(stderr, "invalid number\n");
      return;
    }

  set_errno(err);

  genradio_setchannel(fd, chan);
}

/****************************************************************************
 * Name: gr_mod
 *
 * Description:
 *   Define the modulation type of the generic radio device.
 *
 ****************************************************************************/

static void gr_mod(int fd, char *arg)
{
  int i;
  for(i=0;i<RADIO_MOD_COUNT;i++)
    {
      if(!strcmp(g_modnames[i], arg))
        {
          int ret;
          ret = genradio_setmodulation(fd, i);
          if(ret<0 && errno == EINVAL)
            {
              fprintf(stderr, "Unsupported modulation: %s\n", arg);
            }
          return;
        }
    }
  fprintf(stderr, "Unkown modulation: %s\n", arg);
  return;
}

/****************************************************************************
 * Name: gr_imod
 *
 * Description:
 *   Define the ASK modulation index of the generic radio device.
 *
 ****************************************************************************/

static void gr_imod(int fd, char *arg)
{
  uint32_t imod;
  int err;
  char *endptr;

  err = errno;
  set_errno(0);
  
  imod = strtoul(arg, &endptr, 10);

  if(errno != 0)
    {
      fprintf(stderr, "invalid number\n");
      return;
    }

  set_errno(err);

  if(imod>1000000)
    {
      fprintf(stderr, "error: max value 1 000 000 ppm\n");
      return;
    }

  genradio_setmodulationindex(fd, imod); /* ppm */
}

/****************************************************************************
 * Name: gr_dev
 *
 * Description:
 *   Define the FSK deviation of the generic radio device.
 *
 ****************************************************************************/

static void gr_dev(int fd, char *arg)
{
  uint32_t freq;
  int err;
  char *endptr;

  err = errno;
  set_errno(0);
  
  freq = strtoul(arg, &endptr, 10);

  if(errno != 0)
    {
      fprintf(stderr, "invalid number\n");
      return;
    }

  set_errno(err);

  if(*endptr && *endptr=='k')
    {
      freq *= 1000;
    }

  if(freq>4000000)
    {
      fprintf(stderr, "error: max value 4 MHz\n");
      return;
    }

  genradio_setdeviation(fd, freq*1000); /* mHz */
}

/****************************************************************************
 * Name: gr_rxbw
 *
 * Description:
 *   Define the rx bandwidth of the generic radio device.
 *
 ****************************************************************************/

static void gr_rxbw(int fd, char *arg)
{
  uint32_t freq;
  int err;
  char *endptr;

  err = errno;
  set_errno(0);
  
  freq = strtoul(arg, &endptr, 10);

  if(errno != 0)
    {
      fprintf(stderr, "invalid number\n");
      return;
    }

  set_errno(err);

  if(*endptr && *endptr=='k')
    {
      freq *= 1000;
    }

  if(freq>4000000)
    {
      fprintf(stderr, "error: max value 4 MHz\n");
      return;
    }

  genradio_setrxbandwidth(fd, freq*1000); /* mHz */
}

/****************************************************************************
 * Name: gr_rate
 *
 * Description:
 *   Define the datarate of the generic radio device.
 *
 ****************************************************************************/

static void gr_rate(int fd, char *arg)
{
  uint32_t rate;
  int err;
  char *endptr;

  err = errno;
  set_errno(0);
  
  rate = strtoul(arg, &endptr, 10);

  if(errno != 0)
    {
      fprintf(stderr, "invalid number\n");
      return;
    }

  set_errno(err);

  if(*endptr && *endptr=='k')
    {
      rate *= 1000;
    }

  if(*endptr && *endptr=='M')
    {
      rate *= 1000000;
    }

  if(rate>4000000)
    {
      fprintf(stderr, "error: max value 4 Mbps\n");
      return;
    }

  genradio_setdatarate(fd, rate*1000); /* mbps */
}

/****************************************************************************
 * Name: gr_txpw
 *
 * Description:
 *   Define the transmit power of the generic radio device.
 *
 ****************************************************************************/

static void gr_txpw(int fd, char *arg)
{
  int32_t txpw;
  int err;
  char *endptr;

  err = errno;
  set_errno(0);

  txpw = strtol(arg, &endptr, 10);

  if(errno != 0)
    {
      fprintf(stderr, "invalid number\n");
      return;
    }

  set_errno(err);

  genradio_settxpower(fd, txpw * 100); /* mBm */
}

/****************************************************************************
 * Name: gr_plen
 *
 * Description:
 *   Define the preamble length.
 *
 ****************************************************************************/

static void gr_plen(int fd, char *arg)
{
  uint32_t plen;
  int err;
  char *endptr;

  err = errno;
  set_errno(0);

  plen = strtoul(arg, &endptr, 10);

  if(errno != 0)
    {
      fprintf(stderr, "invalid number\n");
      return;
    }

  set_errno(err);

  genradio_setpreamblelen(fd, plen); /* bits */
}

/****************************************************************************
 * Name: gr_sync
 *
 * Description:
 *   Define the sync word.
 *
 ****************************************************************************/

static void gr_sync(int fd, char *arg)
{
  int len = strlen(arg);
  bool nibble_high = true;
  int index,i;

  if( (len<<2) > GENRADIO_SYNCWORD_MAX)
    {
      fprintf(stderr, "sync word too long (max %d bits)\n", GENRADIO_SYNCWORD_MAX);
      return;
    }
  index = 0;
  for(i = 0; i < len; i++)
    {
      char digit = arg[i];

      if(digit>='0' && digit<='9')
        digit = digit - '0';
      else if(digit>='a' && digit<='f')
        digit = digit - 'a' + 10;
      else if(digit>='A' && digit<='F')
        digit = digit - 'A' + 10;
      else
        {
        fprintf(stderr, "invalid hex digit '%c'\n", digit);
        return;
        }
      if(nibble_high)
        {
          sw[index] = (digit<<4);
        }
      else
        {
          sw[index] |= digit;
          index++;
        }
      nibble_high = !nibble_high;
    }

  len <<= 2; //make that bits
  genradio_setsyncword(fd, sw, len);
}

/****************************************************************************
 * Name: gr_tx
 *
 * Description:
 *   Transmit a packet
 *
 ****************************************************************************/

static void gr_tx(int fd, char *arg)
{
  int ret = write(fd, arg, strlen(arg));
  printf("write result: %d\n", ret);
}

/****************************************************************************
 * Name: gr_txz
 *
 * Description:
 *   Transmit a long packet made of zeros
 *
 ****************************************************************************/

static void gr_txz(int fd, char *arg)
{
  uint8_t *ptr;

  uint32_t pktlen;
  int err;
  char *endptr;

  err = errno;
  set_errno(0);

  pktlen = strtoul(arg, &endptr, 0);

  if(errno != 0)
    {
      fprintf(stderr, "invalid number\n");
      return;
    }

  set_errno(err);

  if (pktlen > 4096)
    {
      fprintf(stderr, "no more than 4096 bytes\n");
      return;
    }

  ptr = calloc(pktlen,1);
  if (!ptr)
    {
      fprintf(stderr, "could not allocate memory\n");
      return;
    }

  int ret = write(fd, ptr, pktlen);

  free(ptr);

  printf("write result: %d\n", ret);
}


/****************************************************************************
 * Name: gr_nioc
 *
 * Description:
 *   Transmit a custom ioctl with numeric argument. Format cmd:param
 *
 ****************************************************************************/

static void gr_nioc(int fd, char *arg)
{
  uint32_t cmd, param;
  int ret;

  int err;
  char *endptr;

  err = errno;
  set_errno(0);

  cmd = strtoul(arg, &endptr, 0);

  if(errno != 0)
    {
      fprintf(stderr, "invalid number in cmd\n");
      return;
    }

  if(!*endptr || *endptr != ':')
    {
      fprintf(stderr, "missing parameter\n");
      return;
    }
  arg = endptr + 1;

  param = strtoul(arg, &endptr, 0);

  if(errno != 0)
    {
      fprintf(stderr, "invalid number in param\n");
      return;
    }

  set_errno(err);

  printf("cmd %08X param %0X\n", cmd, param);

  ret = ioctl(fd,cmd,param);

  printf("Return value: 0x%08X (%d)\n", ret, ret);
  if(ret<0)
    {
      printf("errno       : %d\n", errno);
    }

  set_errno(err);
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * gr_main
 ****************************************************************************/

#ifdef CONFIG_BUILD_KERNEL
int main(int argc, FAR char *argv[])
#else
int gr_main(int argc, char *argv[])
#endif
{
  char *devname;
  char devnametmpl[]="/dev/gr_";
  int fd;
  int ret  = 0;
  int inst = 2;

  if(argc<2)
    {
      /* no dev name */
      fprintf(stderr, "gr <devname>\n"
                      "No instruction - Display interface state\n"
                      "bfreq [<Hz>|<kHz>k|<MHz>M|<GHz>G] - Set base frequency (chan 0)\n"
                      "chspc [<Hz>|<kHz>k|<MHz>M|<GHz>G] - Set channel frequency step\n"
                      "chan <n> - Set channel index\n"
                      "mod  [fsk|gfsk|4fsk|4gfsk|msk|gmsk|ask|ook|bpsk|qpsk|lora - Set modulation\n"
                      "imod <ppm> - Set ASK modulation index\n"
                      "dev  <Hz> - Set FSK deviation\n"
                      "rxbw [<Hz>|<kHz>k] - Set receiver bandwidth\n"
                      "rate [<bps>|<kbps>k|<Mbps>M] - Set data rate\n"
                      "txpw [dB] - Set TX power\n"
                      "plen [bits] - Set preamble length\n"
                      "sync [hexdigits] - Set sync word in multiple of 4 bits\n"
                      "\n"
                      "Report bugs to <sebastien@lorquet.fr>\n"
                      );
      return 1;
    }

  /* we have a dev name */
  devname = argv[1];

  /* Handle simplified dev names noted by single digit */
  if (strlen(devname)==1)
    {
      devnametmpl[7] = devname[0];
      devname = devnametmpl;
      fprintf(stderr, "Real device: %s\n", devname);
    }

  fd = open(devname, O_RDWR);

  if (fd<0)
    {
      fprintf(stderr, "Cannot open: %s\n", devname);
      return 1;
    }

  if (argc<3)
    {
      /* No instruction, display status */
      ret = gr_status(devname, fd);
      close(fd);
      return ret;
    }

  while (argc>3)
    {
      printf("gr tool, instruction: %s arg: %s\n",argv[inst], argv[inst+1]);
      if      (!strcmp(argv[inst], "bfreq"))  gr_freq (fd, argv[inst+1]);
      else if (!strcmp(argv[inst], "chspc" )) gr_chspc(fd, argv[inst+1]);
      else if (!strcmp(argv[inst], "chan" ))  gr_chan (fd, argv[inst+1]);
      else if (!strcmp(argv[inst], "mod" ))   gr_mod  (fd, argv[inst+1]);
      else if (!strcmp(argv[inst], "imod"))   gr_imod (fd, argv[inst+1]);
      else if (!strcmp(argv[inst], "dev" ))   gr_dev  (fd, argv[inst+1]);
      else if (!strcmp(argv[inst], "rxbw"))   gr_rxbw (fd, argv[inst+1]);
      else if (!strcmp(argv[inst], "rate"))   gr_rate (fd, argv[inst+1]);
      else if (!strcmp(argv[inst], "txpw"))   gr_txpw (fd, argv[inst+1]);
      else if (!strcmp(argv[inst], "plen"))   gr_plen (fd, argv[inst+1]);
      else if (!strcmp(argv[inst], "sync"))   gr_sync (fd, argv[inst+1]);
      else if (!strcmp(argv[inst], "tx"))     gr_tx   (fd, argv[inst+1]);
      else if (!strcmp(argv[inst], "txz"))    gr_txz  (fd, argv[inst+1]);
      else if (!strcmp(argv[inst], "nioc"))   gr_nioc (fd, argv[inst+1]);
      else printf("%s: unknown instruction\n", argv[inst]);

      argc -=2;
      inst +=2;
    }

  close(fd);
  return 0;
}
