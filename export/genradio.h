/****************************************************************************
 * genradio/export/genradio.h
 *
 *   Copyright (C) 2017 Sebastien Lorquet. All rights reserved.
 *   Author: Sebastien Lorquet<sebastien@lorquet.fr>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#ifndef __GENRADIO_EXPORT_GENRADIO_H
#define __GENRADIO_EXPORT_GENRADIO_H

#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

/* Get or set the base transmission or reception frequency */
int genradio_setbasefreq(int fd, uint32_t Hz);
int genradio_getbasefreq(int fd, uint32_t *Hz);

/* Get or set the channel spacing */
int genradio_setchanspacing(int fd, uint32_t mHz);
int genradio_getchanspacing(int fd, uint32_t *mHz);

/* Get or set the channel index */
int genradio_setchannel(int fd, int32_t chan);
int genradio_getchannel(int fd, int32_t *chan);

/* Get or set the data rate */
int genradio_setdatarate(int fd, uint32_t rate);
int genradio_getdatarate(int fd, uint32_t *rate);

/* Send custom commands to a module */
int genradio_ioctl(int fd, int cmd, unsigned long arg);

/* Get or Set the modulation used by the module */
int genradio_setmodulation(int fd, uint32_t mod);
int genradio_getmodulation(int fd, uint32_t *mod);

/* For ASK modulation, set the modulation index in ppm */
int genradio_setmodulationindex(int fd, uint32_t ppm);
int genradio_getmodulationindex(int fd, uint32_t *ppm);

/* For FSK modulations, set the frequency deviation,
 * in thousands of an hertz */
int genradio_setdeviation(int fd, uint32_t mHz);
int genradio_getdeviation(int fd, uint32_t *mHz);

/* For gaussian filtered modulations, filter BT value, 16.16 fixed point */
int genradio_setfilterbt(int fd, uint32_t bt);
int genradio_getfilterbt(int fd, uint32_t *bt);

/* Define the length of the preamble field in the packet structure */
int genradio_setpreamblelen(int fd, uint32_t len);
int genradio_getpreamblelen(int fd, uint32_t *len);

/* Define the sync word used to recognize the start of valid packets */
int genradio_setsyncword(int fd, uint8_t *data, uint32_t bits);
int genradio_getsyncword(int fd, uint8_t *data, uint32_t *bits);

/* Get or Set the receiver bandwidth */
int genradio_setrxbandwidth(int fd, uint32_t mHz);
int genradio_getrxbandwidth(int fd, uint32_t *mHz);

/* Get the last calculated RSSI in mB (hundredths of a dB) */
int genradio_getrssi(int fd, int32_t *mB);

/* Get the last calculated LQI in arbitrary units */
int genradio_getlqi(int fd, int32_t *lqi);

/* Get or Set the transmitted signal power in mB, hundredths of a dB) */
int genradio_settxpower(int fd, int32_t mBm);
int genradio_gettxpower(int fd, int32_t *mBm);

#undef EXTERN
#ifdef __cplusplus
}
#endif

#endif /* __GENRADIO_EXPORT_GENRADIO_H */

