NuttX apps for generic radio

This repository contains NuttX applications to test the generic radio drivers.

 * exports: Public include directory, to be linked at $(APPS)/include/genradio
 * libgenradio: Wrapper routines around genradio char driver ioctls
 * gr: test tool, to configure radio devices and test developments

by Sebastien Lorquet, March 20, 2017

